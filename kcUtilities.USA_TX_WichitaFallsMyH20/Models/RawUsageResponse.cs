﻿namespace kcUtilities.USA_TX_WichitaFallsMyH20.Models
{
    public class RawUsageResponse
    {
        #region Public Properties

        public RawUsageData data { get; set; }

        public object[] errors { get; set; }

        public bool operationSuccess { get; set; }

        #endregion Public Properties

        #region Public Classes

        public class RawUsageData
        {
            #region Public Properties

            public string deviceId { get; set; }
            public long earliest { get; set; }
            public long end { get; set; }
            public bool hasNext { get; set; }
            public bool hasPrev { get; set; }
            public long latest { get; set; }
            public string rainUnit { get; set; }
            public bool showToday { get; set; }
            public long start { get; set; }
            public string tempUnit { get; set; }
            public long[] ticks { get; set; }
            public object[][] usage { get; set; }
            public string zoom { get; set; }

            #endregion Public Properties
        }

        #endregion Public Classes
    }
}