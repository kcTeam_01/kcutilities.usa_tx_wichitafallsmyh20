﻿namespace kcUtilities.USA_TX_WichitaFallsMyH20.Models
{
    public class RawInitResponse
    {
        #region Public Properties

        public Defaultunits defaultUnits { get; set; }

        public string googleMapsKey { get; set; }

        public bool hasTerms { get; set; }

        public string[] languages { get; set; }

        public Lists lists { get; set; }

        public Localeversion localeVersion { get; set; }

        public bool pendingEmailChange { get; set; }

        public bool pendingPhoneChange { get; set; }

        public Resources resources { get; set; }

        public bool showDisclaimers { get; set; }

        public bool ssoEnabled { get; set; }

        public Uiproperties uiProperties { get; set; }

        public Usersettings userSettings { get; set; }

        public Utilitylinks utilityLinks { get; set; }

        public string utilityName { get; set; }

        public string utilityPhone { get; set; }

        public string utilityTheme { get; set; }

        public string utilityTimezone { get; set; }

        public Widgetorder[] widgetOrder { get; set; }

        #endregion Public Properties

        #region Public Classes

        public class Account
        {
            #region Public Properties

            public string accountId { get; set; }
            public string[] commodities { get; set; }
            public string[] userRoles { get; set; }

            #endregion Public Properties
        }

        public class Defaultunits
        {
            #region Public Properties

            public string electric { get; set; }
            public string gas { get; set; }
            public string rain { get; set; }
            public string temp { get; set; }
            public string water { get; set; }

            #endregion Public Properties
        }

        public class Lists
        {
        }

        public class Localeversion
        {
            #region Public Properties

            public string en_US { get; set; }
            public string es_MX { get; set; }

            #endregion Public Properties
        }

        public class Resources
        {
            #region Public Properties

            public string en_US { get; set; }
            public string es_MX { get; set; }

            #endregion Public Properties
        }

        public class Uiproperties
        {
            #region Public Properties

            public string sessionId { get; set; }
            public string utility { get; set; }

            #endregion Public Properties
        }

        public class Usersettings
        {
            #region Public Properties

            public Account[] accounts { get; set; }
            public string language { get; set; }
            public bool observer { get; set; }
            public object timeZone { get; set; }
            public string userName { get; set; }
            public string utilityName { get; set; }

            #endregion Public Properties
        }

        public class Utilitylinks
        {
            #region Public Properties

            public string paybill { get; set; }
            public string support { get; set; }
            public string utility { get; set; }

            #endregion Public Properties
        }

        public class Widgetorder
        {
            #region Public Properties

            public string commodity { get; set; }
            public string[] widgetOrderList { get; set; }

            #endregion Public Properties
        }

        #endregion Public Classes
    }
}