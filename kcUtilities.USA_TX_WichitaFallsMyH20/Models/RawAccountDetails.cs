﻿namespace kcUtilities.USA_TX_WichitaFallsMyH20.Models
{
    public class RawAccountDetails
    {
        #region Public Properties

        public Account account { get; set; }
        public int alertCount { get; set; }
        public string[] deviceIdList { get; set; }
        public string[] messageInfoList { get; set; }
        public string[] messageIterator { get; set; }
        public string[] messageList { get; set; }
        public bool operationSuccess { get; set; }

        #endregion Public Properties

        #region Public Classes

        public class Account
        {
            #region Public Properties

            public string accountNumber { get; set; }
            public Address address { get; set; }
            public object currentAccountBal { get; set; }
            public string customerName { get; set; }
            public string serviceType { get; set; }
            public object totAccountBal { get; set; }

            #endregion Public Properties
        }

        public class Address
        {
            #region Public Properties

            public string addressLine1 { get; set; }
            public object addressLine2 { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string zip { get; set; }

            #endregion Public Properties
        }

        #endregion Public Classes
    }
}