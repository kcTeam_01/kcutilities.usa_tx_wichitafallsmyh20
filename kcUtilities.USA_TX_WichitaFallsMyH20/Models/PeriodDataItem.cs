﻿using System;

namespace kcUtilities.USA_TX_WichitaFallsMyH20.Models
{
    public class PeriodDataItem
    {
        #region Public Properties

        public double CubicFeetWaterUsage { get; set; }
        public DateTimeOffset Period { get; set; }
        public double RainAmount { get; set; }
        public double Temperature { get; set; }

        #endregion Public Properties
    }
}