﻿using System.Collections.Generic;

namespace kcUtilities.USA_TX_WichitaFallsMyH20.Models
{
    public class PeriodDataResponse
    {
        #region Public Properties

        public string ErrorMessage { get; set; }
        public string RainUnit { get; set; }
        public List<PeriodDataItem> Readings { get; set; }
        public bool Success { get; set; }
        public string TempUnit { get; set; }

        #endregion Public Properties
    }
}