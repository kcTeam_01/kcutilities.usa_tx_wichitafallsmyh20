﻿using System.Net;
using System.Net.Http;

namespace kcUtilities.USA_TX_WichitaFallsMyH20
{
    internal class CookieAwareClient
    {
        #region Public Fields

        public readonly HttpClient client;

        #endregion Public Fields

        #region Private Fields

        private readonly CookieContainer CookieContainer;

        #endregion Private Fields

        #region Public Constructors

        public CookieAwareClient()
        {
            CookieContainer = new CookieContainer();
            HttpClientHandler handler = new HttpClientHandler
            {
                CookieContainer = CookieContainer,
                UseCookies = true,
                AllowAutoRedirect = true,
                AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip
            };
            client = new HttpClient(handler);

            client.DefaultRequestHeaders.UserAgent.ParseAdd("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36");
            client.DefaultRequestHeaders.Connection.ParseAdd("keep-alive");
            client.DefaultRequestHeaders.CacheControl = new System.Net.Http.Headers.CacheControlHeaderValue() { SharedMaxAge = new System.TimeSpan(0) };
            client.DefaultRequestHeaders.Add("Origin", "https://my-wfall.sensus-analytics.com");
            client.DefaultRequestHeaders.Add("Upgrade-Insecure-Requests", "1");
            client.DefaultRequestHeaders.Accept.ParseAdd("text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
            client.DefaultRequestHeaders.AcceptLanguage.ParseAdd("en-US,en;q=0.9,ga;q=0.8,zh-TW;q=0.7,zh;q=0.6");
        }

        #endregion Public Constructors
    }
}