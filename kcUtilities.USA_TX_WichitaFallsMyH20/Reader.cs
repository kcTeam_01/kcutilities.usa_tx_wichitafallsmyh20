﻿using kcUtilities.USA_TX_WichitaFallsMyH20.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace kcUtilities.USA_TX_WichitaFallsMyH20
{
    public class Reader : IDisposable
    {
        #region Private Fields

        private readonly string _password;

        private readonly string _username;

        private readonly CookieAwareClient client;

        private RawAccountDetails AccountDetails;

        private RawInitResponse InitProperties;

        #endregion Private Fields

        #region Public Constructors

        /// <summary>
        /// Note: The account must already be created on my-wfall.sensus-analytics.com for this to have any chance of working.
        /// </summary>
        /// <param name="Username">The username of an existing account.</param>
        /// <param name="Password">The password associated with the username.</param>
        public Reader(string Username, string Password)
        {
            client = new CookieAwareClient();
            _username = Username;
            _password = Password;
        }

        #endregion Public Constructors

        #region Public Methods

        public void Dispose()
        {
            client?.client?.Dispose();
        }

        /// <summary>
        /// Attempts to login to your account, must be called before attempting to read individual dates.
        /// </summary>
        /// <returns>RawAccountDetails</returns>
        public async Task<RawAccountDetails> Login()
        {
            var loginRequest = new HttpRequestMessage(HttpMethod.Post, "https://my-wfall.sensus-analytics.com/j_spring_security_check");
            loginRequest.Headers.Referrer = new Uri("https://my-wfall.sensus-analytics.com/login.html");
            var loginRequest_Body = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("j_username", _username),
                new KeyValuePair<string, string>("j_password", _password)
            };
            loginRequest.Content = new FormUrlEncodedContent(loginRequest_Body);
            var loginResponse = await client.client.SendAsync(loginRequest).ConfigureAwait(false);
            if (loginResponse.IsSuccessStatusCode || (loginResponse.StatusCode == System.Net.HttpStatusCode.Redirect && loginResponse.Headers.Location.AbsoluteUri == "http://my-wfall.sensus-analytics.com/main.html"))
            {
                InitProperties = await ReadInit(client);
                AccountDetails = await ReadAccountDetails(client);

                return AccountDetails;
            }
            else
            {
                throw new InvalidOperationException($"The response from the server was not the expected value. Expected 'OK', received '{loginResponse.StatusCode}'.");
            }
            throw new InvalidOperationException("Something went wrong.");
        }

        public async Task<JObject> ReadMeterUsage(string DeviceId, int MeterCount)
        {
            var acctDetailsRequest = new HttpRequestMessage(HttpMethod.Post, "https://my-wfall.sensus-analytics.com/water/widget/byPage ");
            acctDetailsRequest.Headers.Referrer = new Uri("https://my-wfall.sensus-analytics.com/main.html");

            acctDetailsRequest.Content = new StringContent(JsonConvert.SerializeObject(new
            {
                accountNumber = InitProperties.userSettings.accounts[0].accountId,
                desktop = true,
                group = "meters",
                deviceId = DeviceId,
                minSize = 4,
                maxSize = 4,
                touch = false,
                meterCount = MeterCount,
            }), System.Text.Encoding.UTF8, "application/json");
            var acctDetailsResponse = await client.client.SendAsync(acctDetailsRequest).ConfigureAwait(false);
            var acctDetailsResponseRaw = await acctDetailsResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
            return JObject.Parse(acctDetailsResponseRaw);
        }

        public async Task<PeriodDataResponse> ReadPeriod(string AccountId, string DeviceId, int Year, int Month, int Day)
        {
            var dtStart = new DateTimeOffset(Year, Month, Day, 0, 0, 0, new TimeSpan(0));
            var dtEnd = dtStart.AddDays(1).AddSeconds(-1);
            try
            {
                var Request = new HttpRequestMessage(HttpMethod.Get, $"https://my-wfall.sensus-analytics.com/water/usage/{AccountId}/{DeviceId}?start={dtStart.ToUnixTimeMilliseconds()}&end={dtEnd.ToUnixTimeMilliseconds()}&zoom=day&page=null");
                Request.Headers.Referrer = new Uri("https://my-wfall.sensus-analytics.com/main.html");

                var Response = await client.client.SendAsync(Request).ConfigureAwait(false);
                var ResponseRaw = await Response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var raw = JsonConvert.DeserializeObject<RawUsageResponse>(ResponseRaw);
                if (raw != null)
                {
                    return new PeriodDataResponse()
                    {
                        Success = true,
                        RainUnit = raw.data.rainUnit,
                        TempUnit = raw.data.tempUnit,
                        Readings = raw.data.usage.Skip(1).Select(p => new PeriodDataItem()
                        {
                            Period = DateTimeOffset.FromUnixTimeMilliseconds((long)p[0]),
                            CubicFeetWaterUsage = (p[1] != null) ? (double)p[1] : 0,
                            RainAmount = (p[2] != null) ? (double)p[2] : 0,
                            Temperature = (p[3] != null) ? (double)p[3] : 0,
                        }).ToList()
                    };
                }
                else
                {
                    throw new InvalidCastException("Invalid result deserializing response body");
                }
            }
            catch (Exception ex)
            {
                return new PeriodDataResponse()
                {
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        #endregion Public Methods

        #region Private Methods

        private async Task<RawAccountDetails> ReadAccountDetails(CookieAwareClient client)
        {
            var acctDetailsRequest = new HttpRequestMessage(HttpMethod.Post, "https://my-wfall.sensus-analytics.com/account/details");
            acctDetailsRequest.Headers.Referrer = new Uri("https://my-wfall.sensus-analytics.com/main.html");
            acctDetailsRequest.Content = new StringContent(JsonConvert.SerializeObject(new { accountNumber = InitProperties.userSettings.accounts[0].accountId, meterTypeByValue = "water" }), System.Text.Encoding.UTF8, "application/json");
            var acctDetailsResponse = await client.client.SendAsync(acctDetailsRequest).ConfigureAwait(false);
            var acctDetailsResponseRaw = await acctDetailsResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
            return JsonConvert.DeserializeObject<RawAccountDetails>(acctDetailsResponseRaw);
        }

        private async Task<RawInitResponse> ReadInit(CookieAwareClient client)
        {
            var initRequest = new HttpRequestMessage(HttpMethod.Post, "https://my-wfall.sensus-analytics.com/init/init");
            initRequest.Headers.Referrer = new Uri("https://my-wfall.sensus-analytics.com/main.html");
            initRequest.Content = new StringContent("{}", System.Text.Encoding.UTF8, "application/json");
            var initResponse = await client.client.SendAsync(initRequest).ConfigureAwait(false);
            var initResponseRaw = await initResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
            return JsonConvert.DeserializeObject<RawInitResponse>(initResponseRaw);
        }

        #endregion Private Methods
    }
}