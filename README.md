
# README #

### What is this repository for? ###

The goal of this package is to provide a simple way to pull down the daily usage data from the MyH2O portal for City of Wichita Falls water customers. 

### How do I get going? ###
1) To use this library you need to have an existing account with https://my-wfall.sensus-analytics.com/login.html

### Code Example ###
    using (var app = new USA_TX_WichitaFallsMyH20.Reader(account.Username, account.Password))
    {
        // You must call Login before other calls
        // This does the actual login

        var result = app.Login().Result;
        if (result != null)
        {
            var LatestRead_Date = DateTimeOffset.UtcNow.Date.AddDays(-2);
            var Period = app.ReadPeriod(result.account.accountNumber, result.deviceIdList[0], LatestRead_Date.Year, LatestRead_Date.Month, LatestRead_Date.Day).Result;
        }
    }